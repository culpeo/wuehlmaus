#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  wuehlmaus.py
#
#  Copyright 2021 Robert Kluj <culpeo@dismail.de>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import requests
import xml.etree.ElementTree

def read_input():
    user_file = input('Enter file name of your query list: ')
    with open(user_file) as input_file:
        input_list = input_file.read().split()
    return input_list

def request_record(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        print('Error while establishing connection to database. Status code:', response.status_code)

def get_content(response):
    content = xml.etree.ElementTree.fromstring(response)
    return content

def get_user_selection():
    user_selection = {}
    user_selection['file name'] = input('Enter name of output file: ')
    user_selection['fasta'] = input('Write protein sequences to FASTA file? [yes/no] (default is "no"): ')
    #user_selection['itol'] = input('Basis for iTOL annotation [gene]- or [entry]-based? (default is no annotation): ')
    return user_selection

def extract_uniprot_data(record, namespace):
    # Errors when requesting information probably caused by errors in
    # localisation (dict 'data'). Contents are expected at certain
    # positions, which were set based on the examined XML structure.
    # Changes by UniProt of the same are therefore likely to cause errors.

    # Set base URL for databases
    kegg_base_url = 'https://www.kegg.jp/entry/'
    interpro_base_url = 'https://www.ebi.ac.uk/interpro/protein/UniProt/'
    string_base_url = 'https://string-db.org/network/'

    data = {
    'accession': './up:accession',
    'entry': './up:name',
    'recommended protein name': './up:protein/up:recommendedName/up:fullName',
    'submitted protein name': './up:protein/up:submittedName/up:fullName',
    'alternative protein name': './up:protein/up:submittedName/up:alternativeName',
    'gene': './up:gene/up:name',
    'protein sequence': './up:sequence',
    'organism': './up:organism/up:name[@type="scientific"]',
    'taxonomy id': './up:organism/up:dbReference',
    'interpro id': './up:dbReference[@type="InterPro"]',
    'interpro descriptions': './up:dbReference[@type="InterPro"]/up:property',
    'kegg id': './up:dbReference[@type="KEGG"]',
    'string id': './up:dbReference[@type="STRING"]'
    }

    # Store extracted data in a dict. If information not found set to 'None'.
    report = {}

    try:
        report['UniProt ID'] = record.find(data['accession'], namespace).text
    except AttributeError:
        report['UniProt ID'] = str(None)

    try:
        report['Entry'] = record.find(data['entry'], namespace).text
    except AttributeError:
        report['Entry'] = str(None)

    try:
        report['Protein name'] = record.find(data['recommended protein name'], namespace).text
    except AttributeError:
        try:
            report['Submitted protein name'] = record.find(data['submitted protein name'], namespace).text
        except AttributeError:
            try:
                report['Alternative protein name'] = record.find(data['alternative protein name'], namespace).text
            except AttributeError:
                report['Protein name'] = str(None)

    try:
        report['Gene'] = record.find(data['gene'], namespace).text
        report['Gene name type'] = record.find(data['gene'], namespace).attrib['type']
    except AttributeError:
        report['Gene'] = str(None)

    try:
        report['Sequence'] = record.find(data['protein sequence'], namespace).text
        report['Length'] = record.find(data['protein sequence'], namespace).attrib['length']
    except AttributeError:
        report['Sequence'] = str(None)

    try:
        report['Organism'] = record.find(data['organism'], namespace).text
        report['Taxonomy ID'] = record.find(data['taxonomy id'], namespace).attrib['id']
    except AttributeError:
        report['Organism'] = str(None)

    try:
        interpro_ids = [item.attrib['id'] for item in record.findall(data['interpro id'], namespace)]
        report['InterPro ID'] = ', '.join(interpro_ids)
        interpro_descriptions = [item.attrib['value'] for item in record.findall(data['interpro descriptions'], namespace)]
        report['InterPro descriptions'] = ', '.join(interpro_descriptions)
    except AttributeError:
        report['InterPro ID'] = str(None)

    try:
        report['InterPro link'] = interpro_base_url + report['UniProt ID']
    except AttributeError:
        report['InterPro link'] = str(None)

    try:
        report['KEGG ID'] = record.find(data['kegg id'], namespace).attrib['id']
    except AttributeError:
        report['KEGG ID'] = str(None)

    try:
        report['KEGG link'] = kegg_base_url + record.find(data['kegg id'], namespace).attrib['id']
    except AttributeError:
        report['KEGG link'] = str(None)

    try:
        report['STRING ID'] = record.find(data['string id'], namespace).attrib['id']
    except AttributeError:
        report['STRING ID'] = str(None)

    try:
        report['STRING link'] = string_base_url + record.find(data['string id'], namespace).attrib['id']
    except AttributeError:
        report['STRING link'] = str(None)

    return report

def extract_ncbi_data(records):
    report = {}
    for record in records:
        tax_id = record.find('./Item[@Name="TaxId"]').text
        report[tax_id] = record.find('./Item[@Name="ScientificName"]').text
    return report

def write_result(report, index, user_selection):
    output_report = user_selection['file name'] + '.tab'
    with open(output_report, 'a') as output_response:
        if index == 1:
            output_response.write('\t'.join(report.keys()) + '\n')
            output_response.write('\t'.join(report.values()) + '\n')
        else:
            output_response.write('\t'.join(report.values()) + '\n')

    if user_selection['fasta'] == 'yes':
        output_sequences = user_selection['file name'] + '.fasta'
        with open(output_sequences, 'a') as output_fasta:
            output_fasta.write('>' + report['UniProt ID'] + '\n' + report['Sequence'] + '\n')

    #if user_selection['itol'] == 'gene':
    #    with open('iTOL base annotation.txt', 'a') as output_itol:
    #        output_itol.write(report['UniProt ID'] + ',' + report['Gene'] + ' (' + report['Organism'] + ')' + '\n')
    #elif user_selection['itol'] == 'entry':
    #    with open('iTOL base annotation.txt', 'a') as output_itol:
    #        output_itol.write(report['UniProt ID'] + ',' + report['Entry'] + ' (' + report['Organism'] + ')' + '\n')

def process_data(input_list, user_selection):
    # Set UniProt's namespace
    namespace = {'up': 'http://uniprot.org/uniprot'}

    # Connect to UniProt
    print('\n' + 'Connecting to UniProt database.')
    url = 'https://www.ebi.ac.uk/proteins/api/proteins?accession=' + '%2C'.join(input_list)
    response = request_record(url)
    content = get_content(response)
    records = content.findall('./up:entry', namespace)
    taxa = content.findall('./up:entry/up:organism/up:dbReference', namespace)
    tax_ids = [tax_id.attrib['id'] for tax_id in taxa]
    tax_ids_unique = set(tax_ids)
    print('Received', str(len(records)), 'of', str(len(input_list)), 'requested record(s) from UniProt.')

    # Connect to NCBI for current taxonomic annotations
    print('\n' + 'Connecting to NCBI database.')
    url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=taxonomy&id=' + ','.join(tax_ids_unique)
    response = request_record(url)
    tax_records = get_content(response)
    tax_report = extract_ncbi_data(tax_records)
    print('Received', str(len(tax_report)), 'of', str(len(tax_ids_unique)), 'requested taxonomic record(s) from NCBI.')

    for index, record in enumerate(records, start = 1):
        report = extract_uniprot_data(record, namespace)
        if report['Taxonomy ID'] in tax_report:
            report['Organism'] = tax_report.get(report['Taxonomy ID'])
        write_result(report, index, user_selection)

    print('\n' + 'Processed all records and wrote report to file.')

input_list = read_input()
user_selection = get_user_selection()
process_data(input_list, user_selection)
