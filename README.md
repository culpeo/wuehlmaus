## Wühlmaus

Wühlmaus is the German name for voles, small rodents that build their underground burrows by digging. In a way, this programme digs through an online database.

### Purpose

This is a simple, small application written in Python. It is used to retrieve information about proteins from the UniProt database using UniProt IDs.

### Use

Wühlmaus is preferably used from within an IDE, as it allows easy inspection and customisation of the source code. Wühlmaus is provided with command line interface (CLI) and grapical user interface (GUI). The CLI version expects the Python packages `requests` and `xml.etree.ElementTree` to be available, the GUI version additionally `tkinter`. For convenience, executable files for Windows are also provided, which are generated using `auto-py-to-exe`. They include all necessary dependencies and can be executed without installation.

The UniProt IDs to be searched for must be provided in a separate file, whereas the CLI version has to be placed within the same folder. The search result is written to a tab-separated text file. There is the option to additionally save the protein sequences in FASTA format in a separate file. For further phylogenetic analyses, a basis for annotation in iTOL can also be provided (only CLI and commented out in the source code by default).

The latest release version can be found in [releases](https://codeberg.org/culpeo/wuehlmaus/releases).

### Further information

Wühlmaus is no longer actively developed. UniProt has revised its ID mapping tool and now allows the extraction of cross-references to other databases. It thus offers the same functions as Wühlmaus, apart from retrieving the current scientific species name from NCBI.
